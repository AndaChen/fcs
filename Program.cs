﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DataProcess;

namespace ConsoleApplication1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            /* variable */
            int paraIndex = 0,paraSize=0,paraCmd=0,paraAddr=0;
            int origRow;
            string ecFileName = "", biosFileName = "", showMsg = "";
            Byte[] FileBuffer = new Byte[4096];
            Byte[] FileHeader = new Byte[256];
            UInt32 bCheckSum = 0;

            Console.WriteLine("\t *=========================================*");
            showMsg = "\t * File Check Sum Console version : " + CodeMain.AppVersion;
            Console.WriteLine(showMsg);
            Console.WriteLine("\t *=========================================*");

            /* program */
            paraCmd = 0;
            for (paraIndex = 0; paraIndex < args.Length; paraIndex++)
            {
                char[] cmd = args[paraIndex].ToCharArray();
                if (cmd[0] == '-')
                {
                    if ((cmd[1] == 's') || (cmd[1] == 'S'))
                    {
                        paraCmd += 1;
                        for (int lindex = 3; lindex < cmd.Length; lindex++)
                        {
                            paraSize *= 10;
                            paraSize += (Convert.ToInt16(cmd[lindex]) - 0x30);
                        }
                    }
                    else if ((cmd[1] == 'g') || (cmd[1] == 'G'))
                    {
                        paraCmd += 2;
                    }
                    else if ((cmd[1] == 'm') || (cmd[1] == 'M'))
                    {
                        paraCmd += 4;
                    }
                    else if ((cmd[1] == 'x') || (cmd[1] == 'X'))
                    {
                        paraCmd += 8;
                        for (int lindex = 3; lindex < cmd.Length; lindex++)
                        {
                            paraAddr *= 16;
                            paraAddr += HexData.CharToInt(cmd[lindex]);
                        }
                        paraAddr &= 0x1000;
                    }
                    else if ((cmd[1] == 'd') || (cmd[1] == 'D'))
                    {
                        paraCmd += 16;
                    }
                }   // scan parameter and filename
            }
                if (paraCmd == 0)
                {
//                    Console.Clear();
                    Console.WriteLine("\t FCS [-s:XXX] [-g] [-x:OOOOOO] EC_File BIOS_File");
                    Console.WriteLine("\t [-s] : EC_File will extend to XXXKB");
                    Console.WriteLine("\t [-g] : EC_File need add CRC32 checksum");
                    Console.WriteLine("\t [-x] : EC_File combine with BIOS at location OOOOOO");
                    Console.WriteLine("\t [-d] : RO/RW bin merge");
                    Console.WriteLine("\t [-m] : EC_File checksum");
                    return;
                }
                else if (paraCmd >= 8)
                {
                    biosFileName = args[args.Length - 1];
                    ecFileName = args[args.Length - 2];
                }
                else
                {
                    ecFileName = args[args.Length - 1];
                }
                /* action 1 : extend EC bin file */
                if ((paraCmd & 0x01) == 0x01)
                {
                    string lFixString = "TAIWAN";
                    string rFileName = ecFileName + ".old";
                    try
                    {
                        File.Copy(ecFileName, rFileName);
                    }
                    catch (IOException copyerror)
                    {
                        showMsg = "\t Warning ! " + copyerror.Message + "\n";
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(showMsg);
                        Console.ForegroundColor = ConsoleColor.White;
                        return;
                    }
                    FileStream rFile = new FileStream(rFileName, FileMode.Open);
                    if (rFile.Length >= (paraSize * 1024))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\t Warning !  the EC file is bigger than extend size,please make small one !");
                        Console.ForegroundColor = ConsoleColor.White;
                        rFile.Close();
                        return;
                    }
                    FileStream wFile = new FileStream(ecFileName, FileMode.Create);
                    long pageSize = paraSize / 4;
 //                   Console.Clear();
                    showMsg = "\t Start extend " + ecFileName + " File to " + Convert.ToString(paraSize) + "KB ";
                    Console.WriteLine(showMsg);
                    origRow = Console.CursorTop;
                    for (int pIndex = 0; pIndex < pageSize; pIndex++)
                    {
                        int bIndex = 0;
                        rFile.Read(FileBuffer, 0, 4096);
                        wFile.Write(FileBuffer, 0, 4096);
                        for (bIndex = 0; bIndex < 4096; bIndex++)
                        {
                            bCheckSum += FileBuffer[bIndex];
                            FileBuffer[bIndex] = 0xFF;
                        }
                        if (pIndex == (pageSize - 2))
                        {
                            bIndex = 0;
                            char[] Carray = lFixString.ToCharArray(0, lFixString.Length);
                            foreach (char c in Carray)
                            {
                                FileBuffer[bIndex + (4095 - lFixString.Length)] = Convert.ToByte(c);
                                bIndex++;
                            }
                        }
                        showMsg = "\t Write File Size : " + HexData.IntToHex(Convert.ToUInt32((pIndex + 1) * 4096), 5);
                        Console.SetCursorPosition(0, origRow);
                        Console.WriteLine(showMsg);
                    }
                    wFile.Close();
                    rFile.Close();
                    showMsg = "\t The " + ecFileName + " Checksum is : " + HexData.IntToHex(Convert.ToUInt16(bCheckSum & 0xFFFF), 4);
                    Console.WriteLine(showMsg);
                }
              
                /* action 2 : google function */
                if ((paraCmd & 0x02) == 0x02)
                {
                    string lFixString = "TAIWAN";
                    string rFileName = ecFileName + ".old";
                    try
                    {
                        File.Copy(ecFileName, rFileName);
                    }
                    catch (IOException copyerror)
                    {
                        showMsg = "\t Warning ! " + copyerror.Message + "\n";
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(showMsg);
                        Console.ForegroundColor = ConsoleColor.White;
                        return;
                    }
                    FileStream rFile = new FileStream(rFileName, FileMode.Open);
                    FileStream wFile = new FileStream(ecFileName, FileMode.Open);
                    showMsg = "\t Start Calibration CRC-32 Checksum ";
                    Console.WriteLine(showMsg);
                    /* get EC Header */
                    rFile.Seek(0x41000, SeekOrigin.Begin);
                    rFile.Read(FileHeader, 0, 256);
                    /* Get Firmware Start Address */
                    UInt32 fw_start = FileHeader[0x17];
                    for (int bIndex = 0; bIndex < 3; bIndex++)
                    {
                        fw_start <<= 8;
                        fw_start += FileHeader[0x16 - bIndex];
                    }
                    fw_start -= 0x20000;
                    /* Get Firmware Size */
                    UInt32 fw_size = FileHeader[0x1B];
                    for (int bIndex = 0; bIndex < 3; bIndex++)
                    {
                        fw_size <<= 8;
                        fw_size += FileHeader[0x1A - bIndex];
                    }
                    fw_size *= 2;
                    rFile.Seek(fw_start, SeekOrigin.Begin);
                    /* CRC-32 Function */
                    bCheckSum = bCheckSum ^ 0xFFFFFFFF;
                    for (int bIndex = 0; bIndex < fw_size; bIndex++)
                    {
                        byte blChecksum = Convert.ToByte(bCheckSum & 0x0FF);
                        UInt32 bhChecksum = bCheckSum >> 8;
                        int bFileData = rFile.ReadByte();
                        bCheckSum = CodeMain.GetCRC32Value(Convert.ToByte((bFileData ^ blChecksum) & 0x0FF));
                        bCheckSum = bCheckSum ^ bhChecksum;
                    }
                    bCheckSum = bCheckSum ^ 0xFFFFFFFF;
                    rFile.Close();
                    showMsg = "\t The " + ecFileName + " RW Area Checksum is : " + HexData.IntToHex(Convert.ToUInt32(bCheckSum), 8);
                    Console.WriteLine(showMsg);
                    /* Write Checksum */
                    wFile.Seek(0x41000, SeekOrigin.Begin);
                    for (int bIndex = 0; bIndex < 4; bIndex++)
                    {
                        FileHeader[0x44 + bIndex] = Convert.ToByte((bCheckSum >> (8 * bIndex)) & 0x0FF);
                    }
                    wFile.Write(FileHeader, 0, 256);
                    int cIndex;
                    for (cIndex = 0; cIndex < 251; cIndex++) FileHeader[cIndex] = 0x0;
                    cIndex = 0;
                    char[] Carray = lFixString.ToCharArray(0, lFixString.Length);
                    foreach (char c in Carray)
                    {
                        FileHeader[cIndex + (255 - lFixString.Length)] = Convert.ToByte(c);
                        cIndex++;
                    }
                    wFile.Seek(0x7FF00, SeekOrigin.Begin);
                    wFile.Write(FileHeader, 0, 256);
                    wFile.Seek(0, SeekOrigin.Begin);
                    for (int pIndex = 0; pIndex < (512 / 4); pIndex++)
                    {
                        int bIndex = 0;
                        wFile.Read(FileBuffer, 0, 4096);
                        for (bIndex = 0; bIndex < 4096; bIndex++)
                        {
                            bCheckSum += FileBuffer[bIndex];
                        }
                    }
                    wFile.Close();
                    showMsg = "\t The " + ecFileName + " Checksum is : " + HexData.IntToHex(Convert.ToUInt16(bCheckSum & 0x0FFFF), 4);
                    Console.WriteLine(showMsg);
                }

                /* action 3 : EC checksum */
                if ((paraCmd & 0x04) == 0x04)
                {
                    FileStream rFile = new FileStream(ecFileName, FileMode.Open);
                    long pageSize = rFile.Length / 1024;
                    showMsg = "\t The " + ecFileName + " size is " + pageSize.ToString() + " KB ";
                    Console.WriteLine(showMsg);

                    origRow = Console.CursorTop;
                    bCheckSum = 0;
                    for (int pIndex = 0; pIndex < pageSize; pIndex++)
                    {
                        int bIndex = 0;
                        rFile.Read(FileBuffer, 0, 1024);
                        for (bIndex = 0; bIndex < 1024; bIndex++)
                        {
                            bCheckSum += FileBuffer[bIndex];
                        }
                    }
                    rFile.Close();
                    showMsg = "\t The " + ecFileName + " Checksum is : " + HexData.IntToHex(Convert.ToUInt16(bCheckSum & 0xFFFF), 4);
                    Console.WriteLine(showMsg);
                }


                /* action 4 : merge BIOS */
                if ((paraCmd & 0x08) == 0x08)
                {
                    FileStream bFile = new FileStream(biosFileName, FileMode.Open);
                    if (paraAddr > bFile.Length)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        showMsg = "\t Warning !  the merge loaction is out of range ! " + HexData.IntToHex(Convert.ToUInt32(paraAddr), 6);
                        Console.WriteLine(showMsg);
                        Console.ForegroundColor = ConsoleColor.White;
                        bFile.Close();
                        return;
                    }
                    showMsg = "\t " + ecFileName + " will merge to " + biosFileName + " at location : " + HexData.IntToHex(Convert.ToUInt32(paraAddr), 6);
                    Console.WriteLine(showMsg);
                    showMsg = "\t " + biosFileName + " File Size is " + Convert.ToString(bFile.Length);
                    Console.WriteLine(showMsg);
                    string wfilename = biosFileName + ".ec";
                    FileStream rFile = new FileStream(ecFileName, FileMode.Open);
                    long ecSize = rFile.Length >> 12;
                    showMsg = "\t " + ecFileName + " File Size is " + Convert.ToString(rFile.Length);
                    Console.WriteLine(showMsg);
                    FileStream wFile = new FileStream(wfilename, FileMode.Create);
                    int mergeIndex = paraAddr >> 12;
                    int wrSize = 0;
                    long pageSize = bFile.Length >> 12;
                    if ((bFile.Length & 0x0FFF) != 0) pageSize += 1;
                    origRow = Console.CursorTop;
                    for (int pIndex = 0; pIndex < pageSize; pIndex++)
                    {
                        wrSize = bFile.Read(FileBuffer, 0, 4096);
                        if ((pIndex >= mergeIndex) && (pIndex < (mergeIndex + ecSize)))
                        {
                            wrSize = rFile.Read(FileBuffer, 0, 4096);
                        }
                        wFile.Write(FileBuffer, 0, wrSize);
                        showMsg = "\t Write File Size : " + HexData.IntToHex(Convert.ToUInt32((pIndex + 1) * 4096), 6);
                        Console.SetCursorPosition(0, origRow);
                        Console.WriteLine(showMsg);
                    }
                    wFile.Close();
                    rFile.Close();
                    bFile.Close();
                }

                /* action 5 : double size of EC for RO/RW */
                if ((paraCmd & 0x10) == 0x10)
                {
                    origRow = Console.CursorTop;
                    FileStream rFile = new FileStream(ecFileName, FileMode.Open);
                    long ecSize = rFile.Length >> 12;
                    showMsg = "\t " + ecFileName + " File Size is " + Convert.ToString(rFile.Length);
                    Console.WriteLine(showMsg);
                    FileStream wFile = new FileStream(biosFileName, FileMode.Create);
                    long pageSize = 0;
                    switch (ecSize / 8)
                    {
                        case 0: pageSize = 16; break;      // Extend to 64K
                        case 1: pageSize = 32; break;      // Extend to 128K
                        case 2: pageSize = 48; break;      // Entend to 192K      
                        case 3: pageSize = 64; break;      // Extend to 256K
                    }

                    origRow = Console.CursorTop;
                    for (int pIndex = 0; pIndex < pageSize; pIndex++)
                    {
                        if (pIndex == (pageSize/2))
                        {
                            rFile.Seek(0, SeekOrigin.Begin);
                        }
                        for (int d = 0; d < 4096; d++)
                            FileBuffer[d] = 0xFF;
                        rFile.Read(FileBuffer, 0, 4096);
                        wFile.Write(FileBuffer, 0, 4096);
                    }
                    Console.SetCursorPosition(0, origRow);
                    showMsg = "\t Write File Size : " + HexData.IntToHex(Convert.ToUInt32(pageSize * 4096), 6);
                    Console.WriteLine(showMsg);
                    wFile.Close();
                    rFile.Close();
                }
            
             Console.WriteLine("\t ******** complete ********");
        } /* main function */
    } /* program */
}
