﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FCS")]
[assembly: AssemblyDescription("File CheckSum")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Quanta NB4 EC")]
[assembly: AssemblyProduct("FCS")]
[assembly: AssemblyCopyright("Copyright © Quanta NB4 2010 - 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("06d427fe-0b78-40b1-b3e2-55012a793bf5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.02.0.0")]
[assembly: AssemblyFileVersion("1.02.2010.1118")]
