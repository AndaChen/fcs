﻿// ******************************************************************************
// * File Name : DateProcess.cs							                        *
// *										                                    *	
// * Copyright (c) 2010 Quanta NB4 software department				            *	
// *										                                    *	
// * Purpose : 	Numeric/String data convertion                		            *
// *										                                    *	
// * Requirements/Functional specifications references:				            *	
// *										                                    *	
// * Development History							                            *	
// *										                                    *	
// * Data	    Version		    Description			                            *	
// * ---------- --------------- -----------------------------------------------	*	
// * 2010/04/20	0.01	        First Release      			                    *	
// *										                                    *	
// ******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataProcess
{
    //---------------------------------------------------------
    //=====================================================================================================================
    //---------------------------------------------------------
    //-- Check Data Format Begin 
    //----------------------------
    class Check
    {
        //---------------------------------------------------------
        //-- Constant declaration
        //-------------------------
        public const char cEnter = (char)13;
        public const char cBackspace = (char)8;
        //------------------------------------------------------------------------------------------------------------------
        public static bool IsNumberic(char cKeyChar)
        {
            bool bIsValidKey = false;
            // cKeyChar == (Char)48 ~ 57 -----> 0~9  
            // cKeyChar == (Char)8 -----------> Backpace  
            // cKeyChar == (Char)13-----------> Enter  
            if ((cKeyChar >= (Char)48 && cKeyChar <= (Char)57) || cKeyChar == cEnter || cKeyChar == cBackspace)
            {
                bIsValidKey = true;
            }
            return bIsValidKey;
        }
    }
    //---------------------------------------------------------
    //-- Check Data Format End 
    //----------------------------
    //=====================================================================================================================
    //---------------------------------------------------------
    //-- Any Data to Numeric Data Format Begin 
    //-----------------------------------------
    class ToNumeric
    {
        //------------------------------------------------------------------------------------------------------------------
        public static UInt32 HexStr2UInt32(String sInputString)
        {
            UInt32 ulData = 0;
            UInt32 ulThisDigit;
            String sThisDigit;
            int iStringSize = sInputString.Length;
            String sString; ;
            if (sInputString.Length >= 2)
            {
                sString = sInputString.Substring(0, 2);
                if (iStringSize > 10)
                {
                    return 0;
                }
                if (sString == "0x" || sString == "0X")
                {
                    for (int iIndex = 2; iIndex < iStringSize; iIndex++)
                    {
                        ulData *= 16;
                        sThisDigit = sInputString.Substring(iIndex, 1);
                        switch (sThisDigit)
                        {
                            case "0":
                                ulThisDigit = 0;
                                break;
                            case "1":
                                ulThisDigit = 1;
                                break;
                            case "2":
                                ulThisDigit = 2;
                                break;
                            case "3":
                                ulThisDigit = 3;
                                break;
                            case "4":
                                ulThisDigit = 4;
                                break;
                            case "5":
                                ulThisDigit = 5;
                                break;
                            case "6":
                                ulThisDigit = 6;
                                break;
                            case "7":
                                ulThisDigit = 7;
                                break;
                            case "8":
                                ulThisDigit = 8;
                                break;
                            case "9":
                                ulThisDigit = 9;
                                break;
                            case "A":
                            case "a":
                                ulThisDigit = 10;
                                break;
                            case "B":
                            case "b":
                                ulThisDigit = 11;
                                break;
                            case "C":
                            case "c":
                                ulThisDigit = 12;
                                break;
                            case "D":
                            case "d":
                                ulThisDigit = 13;
                                break;
                            case "E":
                            case "e":
                                ulThisDigit = 14;
                                break;
                            case "F":
                            case "f":
                                ulThisDigit = 15;
                                break;
                            default:
                                ulThisDigit = 0;
                                break;
                        }
                        ulData += ulThisDigit;
                    }
                }
            }           
            return ulData;
        }
    }
    //---------------------------------------------------------
    //-- Any Data to Numeric Data Format End 
    //-----------------------------------------
    //=====================================================================================================================
    //---------------------------------------------------------
    //-- Any Data to String Data Format Begin 
    //----------------------------------------
    class ToString
    {
        //------------------------------------------------------------------------------------------------------------------
        public static String UInt32ToHexStr(ulong ulValue)
        {
            String sHex = "";
            sHex = Convert.ToString((Int64)ulValue, (Int32)16);
            return sHex;
        }
        //------------------------------------------------------------------------------------------------------------------
        public static String Prefix(String sOrgString, String sMask, int iLength)
        {
            String sOutputString = "";
            String sPrefix = "";
            for (int i = sOrgString.Length; i < iLength; i++)
            {
                sPrefix += sMask;
            }

            sOutputString = sPrefix + sOrgString;
            return sOutputString;
        }
        //------------------------------------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------
    //-- Any Data to String Data Format End 
    //----------------------------------------
    
    class HexData
    {
        public static String IntToHex(UInt32 iHex, int iLen)
        {
            int hIndex = 0;
            String sHex = "";
            for (hIndex = 0; hIndex < iLen; hIndex++)
            {
                if (iHex > 0)
                {
                    sHex = Convert.ToString((iHex & 0x0F), 16).ToUpper() + sHex;
                }
                else
                {
                    sHex = "0" + sHex;
                }
                iHex >>= 4;
            }   
             return sHex;
        }

        public static int CharToInt(char letter)
        {
            int iresult = Convert.ToInt16(letter);

            if ((iresult >= 0x30) && (iresult <= 0x39))
            {
                iresult -= 0x30;
            }
            else if ((iresult >= 0x41) && (iresult <= 0x46))
            {
                iresult -= 0x37;
            }
            else if ((iresult >= 0x61) && (iresult <= 0x66))
            {
                iresult -= 0x57;
            }
            else
            {
                iresult = 0;
            }           
            return iresult;
        }

        public static Int64 HexToInt(String HexString)
        {
            Int64 lResult = 0;
            char[] values = HexString.ToCharArray();
            foreach (char letter in values)
            {
                // Get the integral value of the character.
                int value = CharToInt(letter);
                lResult = (lResult * 16) + value;
            }
            return lResult;
        } 
        static char[] charbuf = new char[256];
        public static String AsciiToString(int aIndex, byte aValue)
        {
            String aString = "";
            charbuf[aIndex] = Convert.ToChar(aValue);
            charbuf[aIndex+1] = '\0';
            aString = new string(charbuf);
            return aString;
        }
    }
}
